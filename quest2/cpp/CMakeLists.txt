cmake_minimum_required(VERSION 3.11-3.18)

project(csv2nested)

add_executable(csv2nested main.cpp)

target_compile_features(csv2nested PUBLIC cxx_std_20)