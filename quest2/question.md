請將下面 csv 資料格式轉換成巢狀格式，假設每行資料都有兩個 ","，總共三個 field，整理成巢狀結構，用前兩個 field 分類，把前兩個 field 相同的最後一個 field 合并成新的 array field "items"，並新增一個 field 統計個數
EX:
### Input
```csv
vegetable,high,cabbage
fruit,high,orange
vegetable,high,tomato
fruit,high,apple
fruit,low,melon
grocery,low,soy sauce
fruit,low,banana
grocery,high,oil
fruit,low,pineapple
```

### Output
```yaml
vegetable
	high
		count: 2
		items: [cabbage, tomato]
fruit
	high
		count: 2
		items: [orange, apple]
	low
		count: 3
		items: [banana, pineapple, melon]
grocery
	low
		count: 1
		items: [soy sauce]
	high
		count: 1
		items: [oil]
```

