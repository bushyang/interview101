
- 下列程式會不會造成 memory leak，該如何解決

```Cpp
int main()
{
	vector<int> vec {1, 2, 4};
	return 0;
}
```
- 如何避免 allocate 的記憶體忘記釋放，常用的方式有哪些
- Smart pointer unique_ptr 的作用是什麽？是爲了解決什麽問題
- 解釋何謂 pure virtual class ，以及其用途
- 程式執行過程中發出 exception 沒被 try catch 住會發生什麽事
- auto 作用是什麽，在什麽情況中使用
- 是否有使用其他程式語言呼叫 c/c++ library 的經驗，或是有其他整合手法 ？
- Copy constructor 和 assignment constructor 作用是什麽？
- C++ 如何管理多執行序列，有用過什麽 stl 或是其他框架管理多執行序的經驗？